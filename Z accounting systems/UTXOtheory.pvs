% This PVS is derived (according to "methods" in the example given in
% "FromZtoPVS" and "PVSaccounting.pvs")

UTXOaccounting : THEORY

BEGIN

TRANSACTIONNAME, ACCOUNTNO, DATE, TEXT, CASH : TYPE

TransInput: TYPE =  [# transaction : TRANSACTIONNAME, spender : ACCOUNTNO #]

% Something to prove here is that an input only refers to a previous transaction.

% Any outputs from a transaction say how much cash they contain and who owns it

TransOutput : TYPE =  [# name : TRANSACTIONNAME, cash : CASH, owner : ACCOUNTNO #]

% Formalising
%\begin{schema}{Transaction}
%name : TRANSACTIONNAME\\
%inputs : \power TransInput\\
%outputs : \power TransOutput
%\where
%\forall o : outputs @ o.name = name
%\end{schema}

TransactionType : TYPE = [# name : TRANSACTIONNAME,
                            inputs : setof[TransInput],
			    outputs : setof[TransOutput] #]

Transaction : setof[TransactionType] = {t : TransactionType | FORALL (o : (t`outputs)) : o`name = t`name }

BCStateType : TYPE = finseq[TransactionType]

BCState : setof[BCStateType] = { b : BCStateType | TRUE}

%ItoO(h, <>)  =_{def} \emptyset\\
%ItoO(h, < h'> \cat t) =_{def} ItoO(h, t) \cup\\
%\t2 \bigcup_{i \in h.inputs}  \{ c : CASH | i.transaction = h'.name \land \\
%\t3 \exists o' : TransOutput | o' \in h'.outputs @ i.spender = o'.owner \land o'.cash = c @ \\
%\t4 \lbind  name \bindsto h'.name, cash \bindsto c, owner \bindsto i.spender \rbind \}
 

ItoO(h : TransactionType, bc : BCStateType) : RECURSIVE setof[TransOutput] =
   IF bc = empty_seq
   THEN emptyset
   ELSE union(ItoO(h, ^(bc, (2,bc`length))),
              IUnion(LAMBDA (i : (h`inputs)):  {o : TransOutput | i`transaction = bc(1)`name AND
                                                                  member(o,bc(1)`outputs) AND
					                          i`spender = o`owner}))
   ENDIF
   MEASURE bc`length

%unspentOutputs\ [h] =_{def} h.outputs\\
%unspentOutputs\ (h : t) =_{def} unspentOutputs(t) \setminus ItoO(h,t)\  \cup\ h.outputs

unspentOutputs (bc : BCStateType) : RECURSIVE setof[TransOutput] =
   IF bc = empty_seq
   THEN emptyset
   ELSE union(difference(unspentOutputs(^(bc, (2,bc`length))), ItoO(bc(1), ^(bc, (2,bc`length)))),
              bc(1)`outputs)
   ENDIF
   MEASURE bc`length

% Now the properties that we must have for a valid UTXO transaction
% First, the cash input and the cash output must be the same
%
% \forall trans : Transaction; i : \nat | i \mapsto trans \in bc @
%              sum_out trans.outputs = sum_in trans.inputs
%
% where sum_out sums all the cash values in the outputs and sum_in sums all the 
% cash values "in" the inputs by referring back to the output that the input
% is formed from in each case:


% Second, all the inputs to the current transaction were amongst
% the outputs of previously occurring valid transactions
%
% \forall trans : Transaction | i \mapsto trans \in bc @
%         \forall input : TransInput | input \in trans.inputs @
%                           input \in unspentOutputs((0..i-1) \domres bc)



END UTXOaccounting
